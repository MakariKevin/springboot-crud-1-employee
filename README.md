A Spring Boot Restful Web Service CRUD - Employee Application

<ins>**Technologies and Tools used:**</ins>
- Spring Boot
- Spring Data JPA
- Maven
- MySQL database

<ins>**Sample Output - Postman:**</ins>

![Screenshot 2022-10-19 at 11.39.55.png](./Screenshot 2022-10-19 at 11.39.55.png)

![Screenshot 2022-10-19 at 11.40.30.png](./Screenshot 2022-10-19 at 11.40.30.png)

![Screenshot 2022-10-19 at 11.40.53.png](./Screenshot 2022-10-19 at 11.40.53.png)

![Screenshot 2022-10-19 at 11.41.11.png](./Screenshot 2022-10-19 at 11.41.11.png)

![Screenshot 2022-10-19 at 11.41.32.png](./Screenshot 2022-10-19 at 11.41.32.png)
